package nl.wur.ssb.hdt2rdf;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public void testApp() throws Exception {
    String[] args = {"--input", "./src/test/resources/example", "--output",
        "./src/test/resources/galaxy.ttl", "--direction", "hdt2rdf"};
    App.main(args);

    String[] args2 = {"--input", "./src/test/resources/test", "--output",
        "./src/test/resources/galaxy.hdt", "--direction", "rdf2hdt"};
    App.main(args2);

  }
}
