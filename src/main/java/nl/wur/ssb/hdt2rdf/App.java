package nl.wur.ssb.hdt2rdf;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdtjena.HDTGraph;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.data.Domain;

public class App {



  private static CommandOptions arguments;



  public static void main(String[] args) throws Exception {

    arguments = new CommandOptions(args);

    if (arguments.direction.contains("hdt2rdf")) {
      org.rdfhdt.hdt.hdt.HDT hdt = org.rdfhdt.hdt.hdt.HDTManager.mapHDT(arguments.inputFile, null);
      HDTGraph graph = new HDTGraph(hdt);
      Model model = ModelFactory.createModelForGraph(graph);
      model = setPrefixes(model);
      OutputStream os = new FileOutputStream(arguments.output);

      System.out.println("HDT2RDF: " + arguments.output);
      model.write(os, "turtle");
    } else if (arguments.direction.contains("rdf2hdt")) {

      RDFSimpleCon tmp = new RDFSimpleCon("file://" + arguments.inputFile + "{TURTLE}");
      Domain domain = new Domain(tmp);
      tmp.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
      tmp.setNsPrefix("protein", "http://csb.wur.nl/genome/protein/");
      tmp.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");

      HDT hdt = new HDT();
      hdt.save(tmp, arguments.output);

    }

  }



  private static Model setPrefixes(Model model) {
    for (String p : App.arguments.prefix.split(" ")) {
      model.setNsPrefix(p.split("@")[0], p.split("@")[1]);
    }
    return model;
  }
}
