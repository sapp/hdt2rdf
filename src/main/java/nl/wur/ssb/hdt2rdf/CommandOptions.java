package nl.wur.ssb.hdt2rdf;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.google.gson.annotations.Expose;

@Parameters(separators = " ")
public class CommandOptions {
  @Parameter(names = "--help", help = true)
  private Boolean help;

  @Expose
  @Parameter(names = {"-i", "-input"}, description = "input file (HDT)", required = true)
  public String inputFile;

  @Expose
  @Parameter(names = {"-o", "-output"}, description = "Output file", required = true)
  public String output;

  @Expose
  @Parameter(names = {"--direction", "-d"}, description = "rdf2hdt or hdt2rdf")
  static String direction;

  @Expose
  @Parameter(names = {"--prefix", "-p"}, description = "Prefix reducer: 'prefix@url prefi2x@url2'")
  static String prefix =
      "ssb@http://csb.wur.nl/genome/ protein@http://csb.wur.nl/genome/protein/ biopax@http://www.biopax.org/release/bp-level3.owl#";


  public CommandOptions(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);
      jc.setAcceptUnknownOptions(true);
      if (this.help != null) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.out.println(pe.getMessage());
      new JCommander(this).usage();
      System.out.println("  * required parameter");
      System.exit(1);
    }
  }
}
