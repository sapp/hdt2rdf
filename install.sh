#!/bin/bash
#============================================================================
#title          :SAPP_module
#description    :module installation script for SAPP
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

if [ "$1" == "test" ]; then
	mvn install -f "$DIR/pom.xml" --quiet
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	mvn install -Dmaven.test.skip=true -f "$DIR/pom.xml" --quiet
fi

mv $DIR/target/*jar $DIR/
