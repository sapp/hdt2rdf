Semantic Annotation Platform with Provenance - HDT2RDF module [![build status](https://gitlab.com/sapp/hdt2rdf/badges/master/build.svg)](https://gitlab.com/sapp/hdt2rdf/commits/master)


CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Maintainers
 
INTRODUCTION
------------

SAPP is a Semantic Annotation Platform with Provenance and is designed on the basis of Semantic Web.
This modules allows you to annotate genomes of various qualities with the full chain of provenance.
Resulting is a RDF genome data model which you can query and analyse using SPARQL.
Various other modules are available which allows you to annotate, visualize and export to various formats.

 * For a full description of each module, visit the project page:
   https://gitlab.com/sapp/

 * To submit bug reports and feature suggestions, or to track changes:
   https://gitlab.com/sapp
   
REQUIREMENTS
------------

This module is depending on JAVA 8 and maven.

INSTALLATION
------------

 * This module can be installed individually using MAVEN or through the provided ./install.sh file inside the module.
 
 * For incorporation into galaxy please use the main SAPP module located [here](https://gitlab.com/sapp/sapp.git)

. 

    git clone https://gitlab.com/sapp/hdt2rdf.git

 
 To start the installation procedure: ./install.sh


    cd hdt2rdf
    ./install.sh
 
 
 
 
MAINTAINERS
-----------

Current maintainers:
 * Jasper Koehorst
 * Jesse van Dam
